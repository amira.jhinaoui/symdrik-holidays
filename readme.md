## Table of contents
* [General info](#general-info)
* [Technologies](#technologies)
* [Setup](#setup)
* [Some Screenshots](#Screenshots)

## General info
This project is simple application for holiday's management in Symdrik Company

## Technologies
Project is created with:
* PHP 7.2
* Symfony 5.2
* EasyAdmin 3.2.8
* VichUploader 1.7
* Mysql

## Setup
To run this project, install it first from git, then to get all the dependencies :

```
$ composer install
```

This may take a while, after that create your database and migrate:

```
$ php bin/console doctrine:database:create
$ php bin/console make:migration
$ php bin/console doctrine:migrations:migrate
```
After creating your database load your fixtures to create your admin user:
```
$ php bin/console doctrine:fixtures:load
```
Now you can connect using your credentials:
- admin@admin.com
- password

## Screenshots
Here's some screenshots
![Login](public/Screenshots/s1.png)
![Login-resp](public/Screenshots/s9.png)
![](public/Screenshots/s2.png)
![](public/Screenshots/s3.png)
![](public/Screenshots/s6.png)
![](public/Screenshots/s7.png)



- etc...



