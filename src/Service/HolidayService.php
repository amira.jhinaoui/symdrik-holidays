<?php
namespace App\Service;
use App\Entity\Holiday;
use App\Repository\HolidayRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query;
use phpDocumentor\Reflection\Types\Array_;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;

class HolidayService
{
    private $client;
    private $em;
    private $holidayRepository;
    private $doctrine;
    public function __Construct(HttpClientInterface $client,EntityManagerInterface  $em,HolidayRepository $holidayRepository){
        $this->client=$client;
        $this->em=$em;
        $this->holidayRepository=$holidayRepository;
    }
    /*public function getHolidayData(): array
    {
        $response= $this->client->request(
            'GET',
            'https://public-holiday.p.rapidapi.com/2021/TN',[
                'headers'=>[ "x-rapidapi-key"=>"f6c89ef235mshc5d71a5a1eb6acdp11332djsnef35d3c1530d",
                "x-rapidapi-host"=> "public-holiday.p.rapidapi.com",
                "useQueryString"=> true,
                    'Accept'=>'application/json',
                    'Content-Type'=>'application/json'],
                'query'=>[
                    'format'=>'json',
                    'inc' => 'date','name'
                ]

            ]
        );

        return $response->toArray();
    }*/
    public function getHolidayPerYear(int $year,string $country):array
    {
        $response= $this->client->request(
            'GET',
            'https://public-holiday.p.rapidapi.com/'.$year.'/'.$country,[
                'headers'=>[ "x-rapidapi-key"=>"f6c89ef235mshc5d71a5a1eb6acdp11332djsnef35d3c1530d",
                    "x-rapidapi-host"=> "public-holiday.p.rapidapi.com",
                    "useQueryString"=> true,
                    'Accept'=>'application/json',
                    'Content-Type'=>'application/json'],
                'query'=>[
                    'format'=>'json',
                    'inc' => 'date','name'
                ]

            ]
        );
        return $response->toArray();
    }

    public function getAlldates() :array{
        $query = $this->em->createQuery(
            'SELECT date
            FROM App\Entity\Holiday 
           '
        );
        return $query->getResult((Query::HYDRATE_ARRAY));
    }





}