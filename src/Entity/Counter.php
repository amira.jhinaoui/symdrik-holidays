<?php

namespace App\Entity;

use App\Repository\CounterRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CounterRepository::class)
 */
class Counter
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $annualCredit;

    /**
     * @ORM\Column(type="integer")
     */
    private $sickCredit;
    /**
     * @ORM\OneToOne(targetEntity="App\Entity\User")
     */
    private $user;

    /**
     * @ORM\Column(type="integer")
     */
    private $year;

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user): void
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * @param mixed $year
     */
    public function setYear($year): void
    {
        $this->year = $year;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAnnualCredit(): ?int
    {
        return $this->annualCredit;
    }

    public function setAnnualCredit(int $annualCredit): self
    {
        $this->annualCredit = $annualCredit;

        return $this;
    }

    public function getSickCredit(): ?int
    {
        return $this->sickCredit;
    }

    public function setSickCredit(int $sickCredit): self
    {
        $this->sickCredit = $sickCredit;

        return $this;
    }
    public function __toString():string
    {
        return $this->id;
    }

}
