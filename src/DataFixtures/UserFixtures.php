<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


class UserFixtures extends Fixture
{
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
   {
        $this->passwordEncoder = $passwordEncoder;
     }
    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user->setFirstName("Admin");
        $user->setLastName("Amin");
        $user->setBirthday(new \DateTime());
        $user->setRoles(["ROLE_ADMIN","ROLE_USER"]);
        $user->setAvatar("avatar");
        $user->setAvatarFile(new \Symfony\Component\HttpFoundation\File\File('public/Uploads/avatar/download.png'));
        $user->setStartDate(new \DateTime());
        $user->setIsActive(1);
        $user->setIsResponsible(0);
        $user->setEmail('admin@admin.com');
        $user->setPassword($this->passwordEncoder->encodePassword(
                       $user,
                        'password'
                   ));
$manager->persist($user);
        $manager->flush();
    }
}
