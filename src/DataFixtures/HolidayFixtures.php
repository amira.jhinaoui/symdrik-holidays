<?php

namespace App\DataFixtures;

use App\Entity\Holiday;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class HolidayFixtures extends Fixture
{
    private $manager;
    private $client;
    public function __construct( HttpClientInterface $client,EntityManagerInterface $manager){
        $this->client=$client;
        $this->manager=$manager;
    }
    public function load(ObjectManager $manager)
    {
       $this->manager=$manager;
       $this->generateHolidays(100);
    }


    private function generateHolidays(int $number):void {

        $holidays=$this->getHolidayData();
        foreach((array)$holidays as $i ){

            $holiday= new Holiday();
            $holiday->setDate($i['date'])
                ->setName($i["name"]);
                $this->manager->persist($holiday);
                $this->manager->flush();
        }
    }
    public function getHolidayData()
    {
        $response= $this->client->request(
            'GET',
            'https://public-holiday.p.rapidapi.com/2021/TN',[
                'headers'=>[ "x-rapidapi-key"=>"f6c89ef235mshc5d71a5a1eb6acdp11332djsnef35d3c1530d",
                    "x-rapidapi-host"=> "public-holiday.p.rapidapi.com",
                    "useQueryString"=> true,
                    ]

            ]

        );

      return $response->toArray();
    }

}
