<?php


namespace App\EventSubscriber;


use App\Controller\Admin\LeaveRequestCrudController;
use App\Entity\Counter;
use App\Entity\LeaveRequest;
use App\Entity\User;
use App\Repository\CounterRepository;

use App\Security\BackendAuthenticator;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\Console\Helper\EntityManagerHelper;
use Doctrine\Persistence\ManagerRegistry;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Event\AfterEntityPersistedEvent;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityUpdatedEvent;
use EasyCorp\Bundle\EasyAdminBundle\Provider\AdminContextProvider;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
/*use Symfony\Component\Security\Core\User\UserProviderInterface;*/
use Symfony\Component\Security\Core\User\UserProviderInterface;


class LeaveSubscriber implements EventSubscriberInterface
{

    private $repository;
    private $backendAuthenticator;
    private $leaveRequestController;
    private  $adminContextProvider;
    private $entityManager;
    public function __construct(CounterRepository $repository,BackendAuthenticator $backendAuthenticator,AdminContextProvider $adminContextProvider,LeaveRequestCrudController $leaveRequestController,EntityManagerInterface $entityManager)
    {
        $this->repository = $repository;
        $this->backendAuthenticator=$backendAuthenticator;
        $this->adminContextProvider=$adminContextProvider;
        $this->leaveRequestController=$leaveRequestController;
        $this->entityManager=$entityManager;

    }

    public static function getSubscribedEvents()
    {
        return [
            AfterEntityPersistedEvent::class => ['changeCounterUser']
        ];
    }


    public function changeCounterUser(AfterEntityPersistedEvent $event)
    {
        $entity = $event->getEntityInstance();
        if (!($entity instanceof LeaveRequest)) {
            return;
        }

        else{

        $counter= $this->repository->findOneBy(['user' => $this->adminContextProvider->getContext()->getUser()]);

        if(!$counter) {
            return;
        }

            if ($entity->getType() == "SICK_LEAVE") {

                if($counter->getSickCredit() <$this->leaveRequestController->countPeriod($entity->getStartDate(), $entity->getEndDate())){
                    $counter->setAnnualCredit($counter->getAnnualCredit()- ( $this->leaveRequestController->countPeriod($entity->getStartDate(), $entity->getEndDate()) - $counter->getSickCredit()));
                    $counter->setSickCredit(0);

                }
                else{
                    $counter->setSickCredit($counter->getSickCredit() - ($this->leaveRequestController->countPeriod($entity->getStartDate(), $entity->getEndDate())));
                }
            }



            else if ($entity->getType() == "ANNUAL_LEAVE") {
                $counter->setAnnualCredit(($counter->getAnnualCredit())- ($this->leaveRequestController->countPeriod($entity->getStartDate(), $entity->getEndDate())));


            $this->entityManager->merge($counter);
            $this->entityManager->flush();
    }

    }

}



}