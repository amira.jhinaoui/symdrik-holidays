<?php

namespace App\Controller\Admin;

use App\Entity\Counter;
use App\Entity\CounterUser;
use App\Entity\Department;
use App\Entity\LeaveRequest;
use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index(): Response
    {
        return parent::index();
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Symdrik RH Conge');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linktoDashboard('Dashboard', 'fa fa-home');
        // yield MenuItem::linkToCrud('The Label', 'fas fa-list', EntityClass::class);
        if($this->isGranted('ROLE_ADMIN')){
            yield MenuItem::linkToCrud('User', 'fas fa-user', User::class);
        }

        yield MenuItem::linkToCrud('Departement', 'fas fa-list', Department::class);
        yield MenuItem::linkToCrud('Counter', 'fas fa-plus', Counter::class);
        yield MenuItem::linkToCrud('Request ', 'fas fa-calendar', LeaveRequest::class);
        yield MenuItem::linkToRoute('Holiday', 'fas fa-calendar', 'holiday');
    }
}
