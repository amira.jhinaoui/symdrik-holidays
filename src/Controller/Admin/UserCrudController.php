<?php

namespace App\Controller\Admin;

use App\Entity\User;
use App\Field\VichImageField;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Field\HiddenField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Vich\UploaderBundle\Form\Type\VichImageType;

class UserCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return User::class;
    }
/*
    public function configureCrud(Crud $crud): Crud
    {
       /* $crud->setEntityPermission('ROLE_ADMIN');
    }*/
    public function configureActions(Actions $actions): Actions
    {
        $actions->setPermission(Action::NEW,'ROLE_ADMIN');
        $actions->setPermission(Action::SAVE_AND_RETURN,'ROLE_ADMIN');
        $actions->setPermission(Action::DELETE,'ROLE_ADMIN');
        $actions->setPermission(Action::EDIT,'ROLE_ADMIN');
        return $actions;

    }

    public function configureFields(string $pageName): iterable
    {
        $imageField = TextareaField::new('avatarFile')
            ->setFormType(VichImageType::class)
            ->setLabel('Image');

        $image = ImageField::new('avatar')
            ->setBasePath("public/Uploads/avatar")
            ->setLabel('Image');

        $fields = [
            TextField::new('firstName'),
            TextField::new('lastName'),
            EmailField::new('email'),
            TextField::new('password')->setFormType(PasswordType::class),
            DateField::new('birthday'),
            DateField::new('startDate'),
            DateField::new('exitDate'),
            BooleanField::new('isResponsible'),
            BooleanField::new('isActive'),
            AssociationField::new( 'department','department'),

        ];

        if ($pageName === Crud::PAGE_INDEX || $pageName === Crud::PAGE_DETAIL) {
            $fields[] = $image;
        } else {
            $fields[] = $imageField;
        }

        return $fields;

    }


}
