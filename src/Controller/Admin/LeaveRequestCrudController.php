<?php

namespace App\Controller\Admin;

use App\Entity\LeaveRequest;
use App\Repository\LeaveRequestRepository;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\FilterCollection;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FieldCollection;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Dto\EntityDto;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Router\CrudUrlGenerator;
use Proxies\__CG__\App\Entity\Counter;

class LeaveRequestCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return LeaveRequest::class;
    }
    public function configureActions(Actions $actions): Actions
    {
        $actions->setPermission(Action::SAVE_AND_ADD_ANOTHER,'ROLE_ADMIN');
        $actions->setPermission(Action::DELETE,'ROLE_ADMIN');
        $actions->setPermission(Action::EDIT,'ROLE_ADMIN');
        $approuveAction = Action::new('Approuve', '')
            ->setIcon('fas fa-check')
            ->linkToCrudAction('approuveAction');
        $actions->setPermission($approuveAction,'ROLE_ADMIN');

        return $actions->add(Crud::PAGE_INDEX, $approuveAction);

    }

    public function countPeriod(\DateTime $dateDebut,\DateTime $dateFin){
        $diff = $dateFin->diff($dateDebut)->format("%a");
        return $diff;
    }

    public function createEntity(string $entityFqcn)
    {
        $leave = new LeaveRequest();
        $leave->setUser($this->getUser());

        return $leave;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            ChoiceField::new('type')->setChoices([
                'SICKNESS' => 'SICK_LEAVE',
                'ANNUAL' => 'ANNUAL_LEAVE',
            ])
                ->allowMultipleChoices(false)
                ->renderExpanded(true),
            DateTimeField::new('startDate','Start Date'),
            DateTimeField::new('endDate','End Date'),
            TextareaField::new('reason','Reason'),
            AssociationField::new('user')->hideOnForm(),
        ];
    }

    public function approuveAction(AdminContext $context)
    {
        $id     = $context->getRequest()->query->get('entityId');
        $entity = $this->getDoctrine()->getRepository(LeaveRequest::class)->find($id);

        $approuve = clone $entity;
        $approuve->setDecision(true);
        $this->persistEntity($this->get('doctrine')->getManagerForClass($context->getEntity()->getFqcn()), $approuve);
        $this->addFlash('success', 'Request Accepted ');

        return $this->redirect($this->get(CrudUrlGenerator::class)->build()->setAction(Action::INDEX)->generateUrl());
    }

}
