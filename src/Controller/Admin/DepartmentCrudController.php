<?php

namespace App\Controller\Admin;

use App\Entity\Department;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class DepartmentCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Department::class;
    }

    public function configureActions(Actions $actions): Actions
    {
        $actions->setPermission(Action::NEW,'ROLE_ADMIN');
        $actions->setPermission(Action::SAVE_AND_RETURN,'ROLE_ADMIN');
        $actions->setPermission(Action::DELETE,'ROLE_ADMIN');
        $actions->setPermission(Action::EDIT,'ROLE_ADMIN');
        return $actions;

    }
    /*public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }*/

}
