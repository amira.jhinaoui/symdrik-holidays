<?php


namespace App\Controller\Admin;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{

    /**
     * @Route ("/login",name="app_login")
     */
    public function login(AuthenticationUtils $authenticationUtils): \Symfony\Component\HttpFoundation\Response
    {
        $error=$authenticationUtils->getLastAuthenticationError();
        $lastUsername=$authenticationUtils->getLastUsername();
        return $this->render('security/login.html.twig',['last_username'=>$lastUsername,'error'=>$error]);
    }
    /**
     * @Route ("/logout",name="app_logout")
     */
    public function logout(AuthenticationUtils $authenticationUtils): \Symfony\Component\HttpFoundation\Response
    {}
}