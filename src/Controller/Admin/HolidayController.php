<?php
namespace App\Controller\Admin;

use App\Service\HolidayService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class HolidayController extends AbstractController{

    /**
     * @Route("/holiday", name="holiday")
     */
    public function index(HolidayService $holidayService):Response{

        return $this->render('calendar.html.twig',['data'=>$holidayService->getHolidayData(2021,"TN") ]);

    }

    /**
     * @Route("/peryear", name="holiday_per_year")
     */
    public function getHolidayByYear(int $year,HolidayService $holidayService,Request $request):Response{
        if($request->isMethod('post')){
            (int)$year=$request->get('search-date');
            return $this->render('calendar.html.twig',['data'=>$holidayService->getHolidayPerYear($year,"TN") ]);
        }
        return $this->render('calendar.html.twig',['data'=>$holidayService->getHolidayData() ]);
    }

}