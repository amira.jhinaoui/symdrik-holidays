<?php

namespace App\Controller\Admin;

use App\Entity\Counter;
use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;

class CounterCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Counter::class;
    }

    public function configureActions(Actions $actions): Actions
    {

        $actions->setPermission(Action::NEW,'ROLE_ADMIN');
        $actions->setPermission(Action::SAVE_AND_RETURN,'ROLE_ADMIN');
        $actions->setPermission(Action::DELETE,'ROLE_ADMIN');
        $actions->setPermission(Action::EDIT,'ROLE_ADMIN');
        return $actions;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            NumberField::new('year','year'),
            AssociationField::new('user','User'),
            NumberField::new('annualCredit'),
            NumberField::new('sickCredit'),
        ];
    }


}
